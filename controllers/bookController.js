var Book = require('../models/book');
var Author = require('../models/author');
var Genre = require('../models/genre');
var BookInstance = require('../models/bookinstance');

exports.index = function(req, res, next) {

    Promise.all([
        new Promise(function(resolve, reject) { // book_count
            Book.countDocuments({}, function(err, count) {  // On passe un objet vide comme conditions de recherche pour tout récupérer
                if (err) {
                    reject(err);
                }
                resolve(count);
            });
        }),
        new Promise(function(resolve, reject) { // book_instance_count
            BookInstance.countDocuments({}, function(err, count) {
                if (err) {
                    reject(err);
                }
                resolve(count);
            });
        }),
        new Promise(function(resolve, reject) { // book_instance_available_count
            BookInstance.countDocuments({status: 'Available'}, function(err, count) {
                if (err) {
                    reject(err);
                }
                resolve(count);
            });
        }),
        new Promise(function(resolve, reject) { // author_count
            Author.countDocuments({}, function(err, count) {
                if (err) {
                    reject(err);
                }
                resolve(count);
            });
        }),
        new Promise(function(resolve, reject) { // genre_count
            Genre.countDocuments({}, function(err, count) {
                if (err) {
                    reject(err);
                }
                resolve(count);
            });
        }),
    ]).then(function(results) {
        console.log(results);
        res.render('index', { 
            title: 'Ma bibliothèque de quartier | Accueil', 
            data: {
                book_count: results[0],
                book_instance_count: results[1],
                book_instance_available_count: results[2],
                author_count: results[3],
                genre_count: results[4]
            }
        });
    }).catch(function(err) {
        next(err);
    });

};

// Affiche la liste de tous les livres.
exports.book_list = function(req, res, next) {

    Book.find({}, 'title author')
        .populate('author')
        .exec(function (err, list_books) {
            if (err) { return next(err); }
            // Succès, on effectue le rendu
            res.render('book_list', { title: 'Liste des livres', book_list: list_books });
        });

};

// Affiche la page de détail pour un livre.
exports.book_detail = function(req, res, next) {

    Promise.all([
        new Promise(function(resolve, reject) { // book
            Book.findById(req.params.id)
              .populate('author')
              .populate('genre')
              .exec(function(err, book) {
                if (err) {
                    reject(err);
                }
                resolve(book);
            });
        }),
        new Promise(function(resolve, reject) { // book_instance
            BookInstance.find({ 'book': req.params.id }).exec(function(err, book_instance) {
                if (err) {
                    reject(err);
                }
                resolve(book_instance);
            });
        })
    ]).then(function(results) {

        if (results[0] == null) { // Pas de résultat => Erreur 404
            var err = new Error('Livre introuvable');
            err.status = 404;
            return next(err);
        }

        res.render('book_detail', {
            title: "Détail d'un livre",
            book: results[0],
            book_instances: results[1]
        });

    }).catch(function(err) {
        next(err);
    });

};

// Display book create form on GET.
exports.book_create_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Book create GET');
};

// Handle book create on POST.
exports.book_create_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Book create POST');
};

// Display book delete form on GET.
exports.book_delete_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Book delete GET');
};

// Handle book delete on POST.
exports.book_delete_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Book delete POST');
};

// Display book update form on GET.
exports.book_update_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Book update GET');
};

// Handle book update on POST.
exports.book_update_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Book update POST');
};